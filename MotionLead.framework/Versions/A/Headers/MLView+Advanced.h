//
//  MLView+Advanced.h
//  MotionLead
//
//  Created by Louis Bur on 12/09/14.
//  Copyright (c) 2014 MotionLead. All rights reserved.
//

#import "MLView.h"

@interface MLView (Advanced)

/**
 *  Initialize an `MLView` to be used to display MotionLead ads with options.
 *  The options keys are specified at the top of MLView.h
 *
 *  @param publisherId      The key provided on your MotionLead account
 *  @param placementId      The placement id for this specific `MLView` in your app
 *  @param options          The options dictionary
 *
 *  @return The newly instanciated `MLView`
 */
- (instancetype)initWithPublisherId:(NSString *)publisherId
                        placementId:(NSString *)placementId
                            options:(NSDictionary *)options;


/**
 *  Display the ad according using the options dictionary.
 *
 *  @param options      The options dictionary, can be nil
 */
- (void)displayAdWithOptions:(NSDictionary *)options;

@end
