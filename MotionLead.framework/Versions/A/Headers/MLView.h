//
//  MLView.h
//  MotionLead
//
//  Created by Louis Bur on 16/08/14.
//  Copyright (c) 2014 MotionLead. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const kMLPublisherId;
extern NSString * const kMLPlacementId;
extern NSString * const kMLAdCustomId;
extern NSString * const kMLAdPackageURL;
extern NSString * const kMLThirdPartyId;
extern NSString * const kMLDisplayURLs;
extern NSString * const kMLClickURLs;
extern NSString * const kMLCloseURLs;
extern NSString * const kMLArgs;

@protocol MLViewDelegate;

#pragma mark - MLView
/**
 *  This UIView subclass allow you to display ads on specific portion of the
 *  screen. It is perfectly adapted to display interstitial-type ads without
 *  blocking the UI.
 */
@interface MLView : UIView

/**
 *  Register to this delegate to receive messages defined in the
 *  `MLViewDelegate` protocol
 */
@property (weak, nonatomic)     id<MLViewDelegate>  delegate;

/**
 *  Get the current MLView displayed on the screen
 *
 *  @return The current MLView
 */
+ (MLView *)currentMLView;

/**
 *  Dismiss the current MLView if available
 */
+ (void)dismiss;

/**
 *  Initialize an `MLView` to be used to display MotionLead ads
 *
 *  @param publisherId      The key provided on your MotionLead account
 *  @param placementId      The placement id for this specific `MLView` in your app
 *
 *  @return The newly instanciated `MLView`
 */
- (instancetype)initWithPublisherId:(NSString *)publisherId
                        placementId:(NSString *)placementId;

/**
 *  Load an ad on the background. It will trigger `mlViewDidLoadAd:` or
 *  `mlViewDidFailToLoadAd:`
 */
- (void)loadAd;

/**
 *  Tells if an ad is waiting and ready to be displayed
 *
 *  @return YES if an ad can be displayed, NO otherwise
 */
- (BOOL)isAdReady;

/**
 *  Tells the `MLView` to display an ad. If no ad is ready to be displayed it
 *  does nothing
 */
- (void)displayAd;

@end

#pragma mark - MLViewDelegate
/**
 *  This protocol allows you to receive additional informations about the state
 *  of the `MLView`.
 */
@protocol MLViewDelegate <NSObject>

@optional
/**
 *  Called when the ad was loaded properly and is now ready to be displayed
 *
 *  @param view The `MLView` which triggered the delegate
 */
- (void)mlViewDidLoadAd:(MLView *)view;

/**
 *  Called when the ad failed to load properly
 *
 *  @param view  The `MLView` which triggered the delegate
 *  @param error An error describing what occured
 */
- (void)mlViewDidFailToLoadAd:(MLView *)view withError:(NSError *)error;

/**
 *  Called when the ad failed to be displayed properly.
 *  Events triggering that delegate:
 *  - An ad is already being displayed
 *  - No ad was loaded before calling `displayAd`
 *  - An error occured in the ad script
 *
 *  @param view  The `MLView` which triggered the delegate
 *  @param error An error describing what occured
 */
- (void)mlViewDidFailToDisplayAd:(MLView *)view withError:(NSError *)error;

/**
 *  Called right before the ad will be displayed
 *
 *  @param view The `MLView` which triggered the delegate
 */
- (void)mlViewWillAppear:(MLView *)view;

/**
 *  Called right after the ad started being displayed
 *
 *  @param view The `MLView` which triggered the delegate
 */
- (void)mlViewDidAppear:(MLView *)view;

/**
 *  Called right before the ad will stop
 *
 *  @param view The `MLView` which triggered the delegate
 */
- (void)mlViewWillDisappear:(MLView *)view;

/**
 *  Called right after the ad stopped
 *
 *  @param view The `MLView` which triggered the delegate
 */
- (void)mlViewDidDisappear:(MLView *)view;

/**
 *  Called if the user clicked on an ad that redirect him outside the
 *  application
 *
 *  @param view The `MLView` which triggered the delegate
 */
- (void)mlViewWillLeaveApplication:(MLView *)view;

/**
 *  Called when the user clicked on an element of the ad that can be counted as
 *  a click-through event
 *
 *  @param view  The `MLView` which triggered the delegate
 */
- (void)mlViewDidReceiveTap:(MLView *)view;

@end

#pragma mark - MLView forbidden methods

/**
 *  These initializer cannot not be called by the application
 */
@interface MLView (Forbidden)

#define ML_UNAVAILABLE_INIT     __attribute__((unavailable("you must use 'initWithPublisherId:placementId:'")))

- (instancetype)init ML_UNAVAILABLE_INIT;
- (instancetype)initWithFrame:(CGRect)frame ML_UNAVAILABLE_INIT;
- (instancetype)initWithCoder:(NSCoder *)aDecoder ML_UNAVAILABLE_INIT;

@end
