//
//  MotionLead.h
//  MotionLead
//
//  Created by Louis Bur on 07/09/14.
//  Copyright (c) 2014 MotionLead. All rights reserved.
//

#define ML_IOS_VERSION      "MotionLead 2.1.10"
#define ML_IOS_VERSION_NUM	2110

#import <Foundation/Foundation.h>

@protocol MotionLeadDelegate;

#pragma mark - MotionLead
/**
 *  The MotionLead class allow you to display MotionLead ad from anywhere in your app. It is 
 *  a collection of class methods and does not need to be instantiated
 */
@interface MotionLead : NSObject

/**
 *  Requests a MotionLead ad. 
 *
 *  This methods does not automatically display the ad
 *
 *  @param publisherId The publisher key of your app
 *  @param placementId The ad placement identifier
 *  @param delegate    A delegate to receive events from the ad
 */
+ (void)requestAdWithPublisherId:(NSString *)publisherId
                     placementId:(NSString *)placementId
                        delegate:(id<MotionLeadDelegate>)delegate;

/**
 *  Requests a MotionLead ad.
 *
 *  This methods does not automatically display the ad
 *
 *  @param publisherId The publisher key of your app
 *  @param placementId The ad placement identifier
 *  @param options     The options dictionary
 *  @param delegate    A delegate to receive events from the ad
 */
+ (void)requestAdWithPublisherId:(NSString *)publisherId
                     placementId:(NSString *)placementId
                         options:(NSDictionary *)options
                        delegate:(id<MotionLeadDelegate>)delegate;

/**
 *  Sets the delegate to receive events from the `MotionLeadDelegate` protocol
 */
+ (void)setDelegate:(id<MotionLeadDelegate>)delegate;

/**
 *  @return YES if an ad is waiting and ready to be displayed, NO otherwise
 */
+ (BOOL)isAdReady;

/**
 *  Display an ad previously downloaded with the methods `requestAdWith...`. If no ad is ready to be
 *  displayed or another ad is currently being displayed, this method does nothing
 */
+ (void)displayAd;

/**
 *  Dismiss the current ad on screen. If no ad is currently being displayed, this method does 
 *  nothing
 */
+ (void)dismissAd;

@end

#pragma mark - MotionLeadDelegate
/**
 *  The MotionLeadDelegate protocol allow your app to be notified of the state of the current ad
 *
 *  @warning *Important:* MotionLead internally stores the delegate weakly. For instance if you have
 *  a chain of view controller like this:
 *      
 *      A -> B -> C
 *  
 *  Where `A` is the root view controller and `B` and `C` are child view controllers. If you request
 *  and ad and set the delegate in `B` and push the view controller `C`, `B` will still receive the 
 *  differents events. If, instead of pushing `C`, you pop the view controller to `A`, `B` will be
 *  deallocated and you will not receive any MotionLead event anymore.
 */
@protocol MotionLeadDelegate <NSObject>

@optional

/**
 *  Called when an ad was properly loaded and is now ready to be displayed
 */
- (void)motionLeadDidLoadAd;

/**
 *  Called when the ad failed to load properly
 *
 *  @param error An error describing what occured
 */
- (void)motionLeadDidFailToLoadAdWithError:(NSError *)error;

/**
 *  Called when the ad failed to be displayed properly.
 *  Events triggering that delegate:
 *  - An ad is already being displayed
 *  - No ad was loaded before calling `displayAd`
 *  - An error occured in the ad script
 *
 *  @param error An error describing what occured
 */
- (void)motionLeadDidFailToDisplayAdWithError:(NSError *)error;

/**
 *  Called right before the ad will be displayed
 */
- (void)motionLeadWillAppear;

/**
 *  Called right after the ad started being displayed
 */
- (void)motionLeadDidAppear;

/**
 *  Called right before the ad will stop
 */
- (void)motionLeadWillDisappear;

/**
 *  Called right after the ad stopped
 */
- (void)motionLeadDidDisappear;

/**
 *  Called if the user clicked on an ad that redirect him outside the
 *  application
 */
- (void)motionLeadWillLeaveApplication;

/**
 *  Called when the user clicked on an element of the ad that can be counted as
 *  a click-through event
 */
- (void)motionLeadDidReceiveTap;

@end
