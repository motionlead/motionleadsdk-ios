Pod::Spec.new do |s|
  s.name                = "MotionLead"
  s.version             = "2.1.10"
  s.summary             = "The official MotionLead iOS SDK"
  s.description         = "A description"
  s.homepage            = "http://motionlead.com"
  s.license             = "Copyright 2015, MotionLead"
  s.author              = { 'MotionLead' => 'http://motionlead.com' }
  s.platform            = :ios, "6.0"
  s.source              = { :git => "https://bitbucket.org/motionlead/motionleadsdk-ios.git", :tag => "2.1.10" }
  s.source_files        = "MotionLead.framework/Versions/A/Headers/*.h"
  s.vendored_frameworks = "MotionLead.framework"
  s.preserve_paths      = "MotionLead.framework"
  s.frameworks          = "AdSupport", "AVFoundation", "CoreMedia", "CoreMotion", "CoreTelephony", "Foundation", "OpenGLES", "QuartzCore", "StoreKit", "SystemConfiguration", "Security", "UIKit"
  s.libraries           = "z", "c++"
  s.requires_arc        = false
  s.xcconfig            = { 'OTHER_LDFLAGS' => '-lObjC' }
end
